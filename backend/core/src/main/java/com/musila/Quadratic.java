package com.musila;

import java.util.*;

class Quadratic
{
    public Quadratic()
    {
        int e,a,b,c;
        double l,x,y;
        Scanner s= new Scanner(System.in);
        System.out.println("Enter value a,b,c:");
        a=s.nextInt();
        b=s.nextInt();
        c=s.nextInt();

        e=b*b-4*a*c;
        if(e<0)
        {
            System.out.println("NOT REAL");
        }
        else if(e == 0)
        {
            x = -b / (2.0 * a);
            System.out.println("The root is " + x);
        }
        else
        {
            l=Math.sqrt(e);
            x= (-b+l)/(2*a);
            y= (-b-l)/(2*a);
            System.out.println("equation= "+x+" & "+y);
        }
    }
}

