package com.musila;


import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class TimetableService extends AbstractVerticle {

    @Override
    public void start(){
          Router r  = Router.router(this.vertx);

        r.get("/").handler(this::ping);

        this.vertx.createHttpServer()
        .requestHandler(r)
        .listen(8080);
        
    }
     private void ping(final RoutingContext rc){
        
        rc.response().end("the man");
     }

    
    
}
